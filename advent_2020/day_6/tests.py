from unittest import TestCase

from advent_2020.day_6.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input_for_anyone(self):
        result = parse_input_for_anyone(test_input)
        expected = [
            {"a", "b", "c"},
            {"a", "b", "c"},
            {"a", "b", "c"},
            {"a"},
            {"b"},
        ]
        self.assertEqual(expected, result)

    def test_count_total_questions(self):
        result = count_total_questions_for_anyone(test_input)
        self.assertEqual(11, result)

    def test_parse_input_for_everyone(self):
        result = parse_input_for_everyone(test_input)
        expected = [
            {"a", "b", "c"},
            set(),
            {"a"},
            {"a"},
            {"b"},
        ]
        self.assertEqual(expected, result)

    def test_count_total_questions(self):
        result = count_total_questions_for_everyone(test_input)
        self.assertEqual(6, result)
