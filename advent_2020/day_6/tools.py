from typing import List, Set


def parse_input_for_anyone(input_text: str) -> List[Set[str]]:
    batches = []
    current_batch = set()
    for line in input_text.splitlines():
        if not line:
            batches.append(current_batch)
            current_batch = set()
        else:
            current_batch.update(set(line))
    batches.append(current_batch)
    return batches


def count_total_questions_for_anyone(input_text: str) -> int:
    questions = parse_input_for_anyone(input_text)
    return sum(map(lambda x: len(x), questions))


def parse_input_for_everyone(input_text: str) -> List[Set[str]]:
    batches = []
    current_batch = None
    for line in input_text.splitlines():
        if not line:
            batches.append(current_batch)
            current_batch = None
        else:
            if current_batch is None:
                current_batch = set(line)
            else:
                current_batch = current_batch.intersection(set(line))
    batches.append(current_batch)
    return batches


def count_total_questions_for_everyone(input_text: str) -> int:
    questions = parse_input_for_everyone(input_text)
    return sum(map(lambda x: len(x), questions))


def handle_part_one(input_text: str) -> int:
    questions = parse_input_for_anyone(input_text)
    return sum(map(lambda x: len(x), questions))


def handle_part_two(input_text: str) -> int:
    questions = parse_input_for_everyone(input_text)
    return sum(map(lambda x: len(x), questions))
