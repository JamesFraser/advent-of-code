from unittest import TestCase

from advent_2020.day_8.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        expected = [
            ("nop", 0),
            ("acc", 1),
            ("jmp", 4),
            ("acc", 3),
            ("jmp", -3),
            ("acc", -99),
            ("acc", 1),
            ("jmp", -4),
            ("acc", 6),
        ]
        self.assertEqual(expected, result)

    def test_console_acc(self):
        console = Console([])
        console.acc(7)
        self.assertEqual(7, console.accumulator)

    def test_console_jmp(self):
        console = Console([])
        console.nop(2)
        self.assertEqual(1, console.index)

    def test_console_nop(self):
        console = Console([])
        console.nop(0)
        self.assertEqual(1, console.index)

    def test_console_debug(self):
        instructions = parse_input(test_input)
        console = Console(instructions)
        console.run(debug_instruction_count=2)

        self.assertEqual(5, console.accumulator)

    def test_generate_mutants(self):
        instructions = parse_input(test_input)
        mutants = generate_mutants(instructions)

        self.assertEqual(3, len(mutants))

    def test_infinite_loop_eg_1(self):
        looping_mutant = [
            ("nop", 0),
            ("acc", 1),
            ("nop", 4),
            ("acc", 3),
            ("jmp", -3),
            ("acc", -99),
            ("acc", 1),
            ("jmp", -4),
            ("acc", 6),
        ]

        result = is_looping_mutant(looping_mutant)
        self.assertEqual(True, result)

    def test_infinite_loop_eg_2(self):
        looping_mutant = [
            ("nop", 0),
            ("acc", 1),
            ("jmp", 4),
            ("acc", 3),
            ("nop", -3),
            ("acc", -99),
            ("acc", 1),
            ("jmp", -4),
            ("acc", 6),
        ]

        result = is_looping_mutant(looping_mutant)
        self.assertEqual(True, result)

    def test_infinite_loop_eg_3(self):
        looping_mutant = [
            ("nop", 0),
            ("acc", 1),
            ("jmp", 4),
            ("acc", 3),
            ("jmp", -3),
            ("acc", -99),
            ("acc", 1),
            ("nop", -4),
            ("acc", 6),
        ]

        result = is_looping_mutant(looping_mutant)
        self.assertEqual(False, result)

    def test_fix_instructions_and_run(self):
        instructions = parse_input(test_input)
        mutant = fix_instructions(instructions)
        console = Console(mutant)
        console.run()
        self.assertEqual(8, console.accumulator)
