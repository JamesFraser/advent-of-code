import re
from typing import List, Tuple

Instruction = Tuple[str, int]


class Console:
    def __init__(self, instructions: List[Instruction]):
        self.accumulator = 0
        self.index = 0
        self.instructions = instructions

    def run(self, debug_instruction_count: int = None) -> None:
        debug_counter = {i: 0 for i in range(len(self.instructions))}
        while self.index < len(self.instructions):
            debug_counter[self.index] += 1

            if debug_instruction_count:
                if debug_instruction_count == debug_counter[self.index]:
                    return

            instruction_map = {
                "acc": self.acc,
                "jmp": self.jmp,
                "nop": self.nop,
            }
            instruction, offset = self.instructions[self.index]
            instruction_map[instruction](offset)

    def acc(self, offset: int):
        self.accumulator += offset
        self.index += 1

    def jmp(self, offset: int):
        self.index += offset

    def nop(self, offset: int):
        self.index += 1


def generate_mutants(instructions: List[Instruction]) -> List[List[Instruction]]:
    mutants = []
    for i, (instruction, offset) in enumerate(instructions):
        if instruction == "acc":
            continue

        if (instruction, offset) == ("nop", 0):
            continue

        copy = instructions.copy()
        if instruction == "jmp":
            copy[i] = ("nop", offset)
        if instruction == "nop":
            copy[i] = ("jmp", offset)

        mutants.append(copy)

    return mutants


def is_looping_mutant(instructions: List[Instruction]) -> bool:
    # TODO: Better logic here.
    c = Console(instructions)
    c.run(2)
    return c.index != len(instructions)


def parse_input(input_text: str) -> List[Tuple[str, int]]:
    instructions = []
    regex = "^(.+?)\s([+-]\d+?)$"
    for line in input_text.splitlines():
        m = re.match(regex, line)
        instruction = m.group(1)
        offset = int(m.group(2))

        instructions.append((instruction, offset))

    return instructions


def fix_instructions(instructions: List[Instruction]):
    mutants = generate_mutants(instructions)
    non_looping_mutants = [
        mutant for mutant in mutants if not is_looping_mutant(mutant)
    ]

    if len(non_looping_mutants) != 1:
        raise Exception(f"Found {len(non_looping_mutants)} mutants")

    return non_looping_mutants[0]


def handle_part_one(input_text: str) -> int:
    instructions = parse_input(input_text)
    console = Console(instructions)
    console.run(debug_instruction_count=2)

    return console.accumulator


def handle_part_two(input_text: str) -> int:
    instructions = parse_input(input_text)
    mutant = fix_instructions(instructions)
    console = Console(mutant)
    console.run()

    return console.accumulator
