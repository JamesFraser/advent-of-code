from unittest import TestCase

from advent_2020.day_7.tools import *

f = open("test_input.txt", "r")
test_input = f.read()

f = open("test_input_2.txt", "r")
test_input_2 = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        expected = {
            "light red": [(1, "bright white"), (2, "muted yellow")],
            "dark orange": [(3, "bright white"), (4, "muted yellow")],
            "bright white": [(1, "shiny gold")],
            "muted yellow": [(2, "shiny gold"), (9, "faded blue")],
            "shiny gold": [(1, "dark olive"), (2, "vibrant plum")],
            "dark olive": [(3, "faded blue"), (4, "dotted black")],
            "vibrant plum": [(5, "faded blue"), (6, "dotted black")],
            "faded blue": [],
            "dotted black": [],
        }
        self.assertEqual(expected, result)

    def test_get_bags_which_could_contain_bag(self):
        mapping = parse_input(test_input)
        result = get_bags_which_could_contain_bag(mapping, "shiny gold")
        expected = {"bright white", "muted yellow", "dark orange", "light red"}
        self.assertEqual(expected, result)

    def test_count_contents_of_bag_eg_1(self):
        mapping = parse_input(test_input)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(0, bag_map["faded blue"])

    def test_count_contents_of_bag_eg_2(self):
        mapping = parse_input(test_input)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(0, bag_map["dotted black"])

    def test_count_contents_of_bag_eg_3(self):
        mapping = parse_input(test_input)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(11, bag_map["vibrant plum"])

    def test_count_contents_of_bag_eg_4(self):
        mapping = parse_input(test_input)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(7, bag_map["dark olive"])

    def test_count_contents_of_bag_eg_5(self):
        mapping = parse_input(test_input)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(32, bag_map["shiny gold"])

    def test_count_contents_of_bag_eg_6(self):
        mapping = parse_input(test_input_2)
        bag_map = get_bags_count_map(mapping)
        self.assertEqual(126, bag_map["shiny gold"])
