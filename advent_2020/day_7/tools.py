import re
from typing import Dict, List, Set, Tuple

Mapping = Dict[str, List[Tuple[int, str]]]


def parse_input(input_text: str) -> Mapping:
    line_regex = "^(.+?) bags contain (.*?)\.$"
    content_regex = "^(\d+?)\s(.*?) bag"

    mapping = {}

    for line in input_text.splitlines():
        m = re.search(line_regex, line)
        bag_colour = m.group(1)
        contains = m.group(2)

        mapping[bag_colour] = []

        if contains == "no other bags":
            continue

        for content in contains.split(", "):
            m = re.search(content_regex, content)
            quantity = int(m.group(1))
            colour = m.group(2)

            mapping[bag_colour].append((quantity, colour))

    return mapping


def get_bags_which_could_contain_bag(
    mapping: Mapping,
    target_colour: str,
    known_bags: Set[str] = set(),
) -> Set[str]:
    bags = set()

    for bag, contains in mapping.items():
        for _, colour in contains:
            if colour == target_colour or colour in known_bags:
                bags.add(bag)

    if bags == known_bags:
        return bags

    return get_bags_which_could_contain_bag(mapping, target_colour, bags)


def get_bags_count_map(mapping: Mapping) -> Dict[str, int]:
    bag_contains_map = {}

    while len(bag_contains_map) != len(mapping):
        for bag, contains in mapping.items():
            if bag in bag_contains_map:
                continue

            if not contains:
                bag_contains_map[bag] = 0

            if set([colour for _, colour in contains]).issubset(
                set(bag_contains_map.keys())
            ):
                total = 0
                for quantity, colour in contains:
                    total += quantity
                    total += quantity * bag_contains_map[colour]
                bag_contains_map[bag] = total

    return bag_contains_map


def handle_part_one(input_text: str) -> int:
    mapping = parse_input(input_text)
    return len(get_bags_which_could_contain_bag(mapping, "shiny gold"))


def handle_part_two(input_text: str) -> int:
    mapping = parse_input(input_text)
    bags_map = get_bags_count_map(mapping)
    return bags_map["shiny gold"]
