from unittest import TestCase

from advent_2020.day_18.tools import *


class TestCases(TestCase):
    def test_evaluate_line_eg_1(self):
        result = evaluate_line("1 + 2 * 3 + 4 * 5 + 6")
        self.assertEqual(71, result)

    def test_evaluate_line_eg_2(self):
        result = evaluate_line("1 + (2 * 3) + (4 * (5 + 6))")
        self.assertEqual(51, result)

    def test_evaluate_line_eg_3(self):
        result = evaluate_line("2 * 3 + (4 * 5)")
        self.assertEqual(26, result)

    def test_evaluate_line_eg_4(self):
        result = evaluate_line("5 + (8 * 3 + 9 + 3 * 4 * 3)")
        self.assertEqual(437, result)

    def test_evaluate_line_eg_5(self):
        result = evaluate_line("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")
        self.assertEqual(12240, result)

    def test_evaluate_line_eg_6(self):
        result = evaluate_line("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
        self.assertEqual(13632, result)

    def test_evaluate_line_part_2_eg_1(self):
        result = evaluate_line("1 + 2 * 3 + 4 * 5 + 6", ignore_precedence=False)
        self.assertEqual(231, result)

    def test_evaluate_line_part_2_eg_2(self):
        result = evaluate_line("1 + (2 * 3) + (4 * (5 + 6))", ignore_precedence=False)
        self.assertEqual(51, result)

    def test_evaluate_line_part_2_eg_3(self):
        result = evaluate_line("2 * 3 + (4 * 5)", ignore_precedence=False)
        self.assertEqual(46, result)

    def test_evaluate_line_part_2_eg_4(self):
        result = evaluate_line("5 + (8 * 3 + 9 + 3 * 4 * 3)", ignore_precedence=False)
        self.assertEqual(1445, result)

    def test_evaluate_line_part_2_eg_5(self):
        result = evaluate_line(
            "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", ignore_precedence=False
        )
        self.assertEqual(669060, result)

    def test_evaluate_line_part_2_eg_6(self):
        result = evaluate_line(
            "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", ignore_precedence=False
        )
        self.assertEqual(23340, result)
