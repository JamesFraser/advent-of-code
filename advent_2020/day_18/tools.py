import re


def parse_input(input_text: str):
    return [line.strip() for line in input_text.splitlines()]


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def evaluate_line(line: str, ignore_precedence: bool = True) -> int:
    regex = "(\([^\(]*?\))"
    while "(" in line:
        m = re.search(regex, line)
        replacement = str(evaluate_expression(m.group(1), ignore_precedence))
        line = line.replace(m.group(1), replacement)
    return evaluate_expression(line, ignore_precedence)


def evaluate_expression(line: str, ignore_precedence: bool = True) -> int:
    line = line.strip("()")

    if "+" in line and "*" in line:
        if not ignore_precedence:
            regex = "(\d+\s\+\s\d+)"

            while "+" in line:
                m = re.search(regex, line)
                replacement = str(evaluate_expression(m.group(1), ignore_precedence))
                line = line.replace(m.group(1), replacement)

    tokens = line.split(" ")

    for i, token in enumerate(tokens):
        try:
            tokens[i] = int(token)
        except:
            pass

    total = tokens.pop(0)
    for operator, operand in chunks(tokens, 2):
        if operator == "+":
            total += operand
        if operator == "*":
            total *= operand
    return total


def handle_part_one(input_text: str) -> int:
    lines = parse_input(input_text)
    return sum([evaluate_line(line) for line in lines])


def handle_part_two(input_text: str) -> int:
    lines = parse_input(input_text)
    return sum([evaluate_line(line, ignore_precedence=False) for line in lines])
