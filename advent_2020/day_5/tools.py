from math import ceil, floor
from typing import Tuple

Range = Tuple[int, int]


def get_halves(lower: int, upper: int) -> Tuple[Range, Range]:
    half = (upper + lower) / 2
    return (lower, floor(half)), (ceil(half), upper)


def determine_half(binary_char: str, front_half: Range, back_half: Range) -> Range:
    if binary_char in ["F", "L"]:
        return front_half
    if binary_char in ["B", "R"]:
        return back_half
    raise Exception


def determine_row(input_str: str) -> int:
    rows = (0, 127)
    for char in input_str:
        front, back = get_halves(*rows)
        rows = determine_half(char, front, back)
    return rows[0]


def determine_column(input_str: str) -> int:
    cols = (0, 7)
    for char in input_str:
        left, right = get_halves(*cols)
        cols = determine_half(char, left, right)
    return cols[0]


def determine_seat(input_str: str) -> Tuple[int, int]:
    row = input_str[:7]
    col = input_str[7:]

    return determine_row(row), determine_column(col)


def determine_seat_id(row: int, col: int) -> int:
    return (row * 8) + col


def find_max_seat_id(input_text: str) -> int:
    max_seat_id = 0
    for line in input_text.splitlines():
        row, col = determine_seat(line)
        seat_id = determine_seat_id(row, col)

        max_seat_id = max(seat_id, max_seat_id)
    return max_seat_id


def handle_part_one(input_text: str) -> int:
    max_seat_id = 0
    for line in input_text.splitlines():
        row, col = determine_seat(line)
        seat_id = determine_seat_id(row, col)

        max_seat_id = max(seat_id, max_seat_id)
    return max_seat_id


def handle_part_two(input_text: str) -> int:
    filled_seat_ids = set()
    for line in input_text.splitlines():
        row, col = determine_seat(line)
        seat_id = determine_seat_id(row, col)

        filled_seat_ids.add(seat_id)

    front = True
    for seat in range(1025):
        if seat in filled_seat_ids:
            front = False
        elif seat not in filled_seat_ids and not front:
            return seat
