from unittest import TestCase

from advent_2020.day_5.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_get_halves_eg_1(self):
        result = get_halves(0, 127)
        self.assertEqual(((0, 63), (64, 127)), result)

    def test_get_halves_eg_2(self):
        result = get_halves(0, 63)
        self.assertEqual(((0, 31), (32, 63)), result)

    def test_get_halves_eg_3(self):
        result = get_halves(32, 63)
        self.assertEqual(((32, 47), (48, 63)), result)

    def test_get_halves_eg_4(self):
        result = get_halves(32, 47)
        self.assertEqual(((32, 39), (40, 47)), result)

    def test_determine_row(self):
        result = determine_row("FBFBBFF")
        self.assertEqual(44, result)

    def test_determine_column(self):
        result = determine_column("RLR")
        self.assertEqual(5, result)

    def test_determine_seat_eg_1(self):
        result = determine_seat("BFFFBBFRRR")
        self.assertEqual((70, 7), result)

    def test_determine_seat_eg_2(self):
        result = determine_seat("FFFBBBFRRR")
        self.assertEqual((14, 7), result)

    def test_determine_seat_eg_3(self):
        result = determine_seat("BBFFBBFRLL")
        self.assertEqual((102, 4), result)

    def test_determine_seat_id_eg_1(self):
        result = determine_seat_id(70, 7)
        self.assertEqual(567, result)

    def test_determine_seat_id_eg_2(self):
        result = determine_seat_id(14, 7)
        self.assertEqual(119, result)

    def test_determine_seat_id_eg_3(self):
        result = determine_seat_id(102, 4)
        self.assertEqual(820, result)

    def test_find_max_seat_id(self):
        result = find_max_seat_id(test_input)
        self.assertEqual(820, result)
