from unittest import TestCase

from advent_2020.day_15.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        self.assertEqual([0, 3, 6], result)

    def test_play_game_eg_1(self):
        memory, last_played = play_game([0, 3, 6], 10)
        expected_memory = {0: 9, 1: 6, 3: 5, 4: 8, 6: 2}
        self.assertEqual(expected_memory, memory)
        self.assertEqual(last_played, 0)

    def test_play_game_eg_2(self):
        _, last_played = play_game([0, 3, 6], 2020)
        self.assertEqual(last_played, 436)

    def test_play_game_eg_3(self):
        _, last_played = play_game([1, 3, 2], 2020)
        self.assertEqual(last_played, 1)

    def test_play_game_eg_4(self):
        _, last_played = play_game([2, 1, 3], 2020)
        self.assertEqual(last_played, 10)

    def test_play_game_eg_5(self):
        _, last_played = play_game([1, 2, 3], 2020)
        self.assertEqual(last_played, 27)

    def test_play_game_eg_6(self):
        _, last_played = play_game([2, 3, 1], 2020)
        self.assertEqual(last_played, 78)

    def test_play_game_eg_7(self):
        _, last_played = play_game([3, 2, 1], 2020)
        self.assertEqual(last_played, 438)

    def test_play_game_eg_8(self):
        _, last_played = play_game([3, 1, 2], 2020)
        self.assertEqual(last_played, 1836)

    def test_play_game_hard_eg_1(self):
        _, last_played = play_game([0, 3, 6], 30000000)
        self.assertEqual(last_played, 175594)

    def test_play_game_hard_eg_2(self):
        _, last_played = play_game([1, 3, 2], 30000000)
        self.assertEqual(last_played, 2578)

    def test_play_game_hard_eg_3(self):
        _, last_played = play_game([2, 1, 3], 30000000)
        self.assertEqual(last_played, 3544142)

    def test_play_game_hard_eg_4(self):
        _, last_played = play_game([1, 2, 3], 30000000)
        self.assertEqual(last_played, 261214)

    def test_play_game_hard_eg_5(self):
        _, last_played = play_game([2, 3, 1], 30000000)
        self.assertEqual(last_played, 6895259)

    def test_play_game_hard_eg_6(self):
        _, last_played = play_game([3, 2, 1], 30000000)
        self.assertEqual(last_played, 18)

    def test_play_game_hard_eg_7(self):
        _, last_played = play_game([3, 1, 2], 30000000)
        self.assertEqual(last_played, 362)
