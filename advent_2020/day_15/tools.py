from typing import Dict, List, Tuple


def parse_input(input_text: str) -> List[int]:
    return [int(i) for i in input_text.split(",")]


def play_game(
    starting_numbers: List[int], turns_to_play: int
) -> Tuple[Dict[int, int], int]:
    memory = {}
    next_number = None
    this_turn = None

    for turn in range(turns_to_play):
        if turn < len(starting_numbers):
            next_number = starting_numbers[turn]

        this_turn = next_number

        if this_turn in memory:
            next_number = turn - memory[this_turn]
        else:
            next_number = 0

        memory[this_turn] = turn

    return memory, this_turn


def handle_part_one(input_text: str) -> int:
    starting_numbers = parse_input(input_text)
    _, result = play_game(starting_numbers, 2020)
    return result


def handle_part_two(input_text: str) -> int:
    starting_numbers = parse_input(input_text)
    _, result = play_game(starting_numbers, 30000000)
    return result
