from typing import List, Tuple

Instruction = Tuple[str, int]

directions = {
    "N": (0, 1),
    "E": (1, 0),
    "S": (0, -1),
    "W": (-1, 0),
}


def parse_input(input_text: str) -> List[Instruction]:
    instructions = []
    for line in input_text.splitlines():
        instructions.append((line[0], int(line[1:])))
    return instructions


def get_vector(instruction: Instruction, current_facing: str):
    direction, units = instruction
    if direction in directions.keys():
        x_delta, y_delta = directions[direction]
        vector = (x_delta * units, y_delta * units)
        return vector, current_facing

    if direction == "F":
        x_delta, y_delta = directions[current_facing]
        vector = (x_delta * units, y_delta * units)
        return vector, current_facing

    if direction == "L":
        turns = ["N", "W", "S", "E"]
    elif direction == "R":
        turns = ["N", "E", "S", "W"]
    else:
        raise Exception(f"Direction {direction} not known")

    index = turns.index(current_facing)
    offset = int(units / 90)

    return (0, 0), turns[(index + offset) % 4]


def process_instructions_with_old_instructions(
    instructions: List[Instruction], facing: str
):
    x, y = 0, 0
    for instruction in instructions:
        (x_delta, y_delta), facing = get_vector(instruction, facing)
        x += x_delta
        y += y_delta

    return x, y


def get_location_and_waypoint(
    instruction: Instruction, ship_location: Tuple[int, int], waypoint: Tuple[int, int]
):
    way_x, way_y = waypoint

    direction, units = instruction

    if direction in directions.keys():
        x_delta, y_delta = directions[direction]
        x_delta *= units
        y_delta *= units
        return ship_location, (way_x + x_delta, way_y + y_delta)

    ship_x, ship_y = ship_location

    if direction == "F":
        x_delta = units * way_x
        y_delta = units * way_y
        return (ship_x + x_delta, ship_y + y_delta), waypoint

    if direction == "L":
        turns = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
    elif direction == "R":
        turns = [(1, 1), (1, -1), (-1, -1), (-1, 1)]
    else:
        raise Exception(f"Direction {direction} not known")

    index = int(units / 90)
    x_scale, y_scale = turns[index]
    if index in [1, 3]:
        way_x, way_y = way_y, way_x
    way_x *= x_scale
    way_y *= y_scale

    return ship_location, (way_x, way_y)


def process_instructions_with_new_instructions(instructions: List[Instruction]):
    location = (0, 0)
    waypoint = (10, 1)

    for instruction in instructions:
        location, waypoint = get_location_and_waypoint(instruction, location, waypoint)

    return location


def handle_part_one(input_text: str) -> int:
    instructions = parse_input(input_text)
    x, y = process_instructions_with_old_instructions(instructions, "E")
    return abs(x) + abs(y)


def handle_part_two(input_text: str) -> int:
    instructions = parse_input(input_text)
    x, y = process_instructions_with_new_instructions(instructions)
    return abs(x) + abs(y)
