from unittest import TestCase

from advent_2020.day_12.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        expected = [
            ("F", 10),
            ("N", 3),
            ("F", 7),
            ("R", 90),
            ("F", 11),
        ]
        self.assertEqual(expected, result)

    def test_get_vector_eg_1(self):
        vector, facing = get_vector(("N", 3), "E")
        self.assertEqual((0, 3), vector)
        self.assertEqual("E", facing)

    def test_get_vector_eg_2(self):
        vector, facing = get_vector(("W", 10), "E")
        self.assertEqual((-10, 0), vector)
        self.assertEqual("E", facing)

    def test_get_vector_eg_3(self):
        vector, facing = get_vector(("F", 5), "S")
        self.assertEqual((0, -5), vector)
        self.assertEqual("S", facing)

    def test_get_vector_eg_4(self):
        vector, facing = get_vector(("R", 90), "S")
        self.assertEqual((0, 0), vector)
        self.assertEqual("W", facing)

    def test_process_instructions_with_old_instructions(self):
        instructions = parse_input(test_input)
        result = process_instructions_with_old_instructions(instructions, "E")
        self.assertEqual((17, -8), result)

    def test_get_location_and_waypoint_eg_1(self):
        instruction = ("F", 10)
        location = (0, 0)
        waypoint = (10, 1)

        new_location, new_waypoint = get_location_and_waypoint(
            instruction, location, waypoint
        )
        self.assertEqual((100, 10), new_location)
        self.assertEqual((10, 1), new_waypoint)

    def test_get_location_and_waypoint_eg_2(self):
        instruction = ("N", 3)
        location = (100, 10)
        waypoint = (10, 1)

        new_location, new_waypoint = get_location_and_waypoint(
            instruction, location, waypoint
        )
        self.assertEqual((100, 10), new_location)
        self.assertEqual((10, 4), new_waypoint)

    def test_get_location_and_waypoint_eg_3(self):
        instruction = ("F", 7)
        location = (100, 10)
        waypoint = (10, 4)

        new_location, new_waypoint = get_location_and_waypoint(
            instruction, location, waypoint
        )
        self.assertEqual((170, 38), new_location)
        self.assertEqual((10, 4), new_waypoint)

    def test_get_location_and_waypoint_eg_4(self):
        instruction = ("R", 90)
        location = (170, 38)
        waypoint = (10, 4)

        new_location, new_waypoint = get_location_and_waypoint(
            instruction, location, waypoint
        )
        self.assertEqual((170, 38), new_location)
        self.assertEqual((4, -10), new_waypoint)

    def test_get_location_and_waypoint_eg_5(self):
        instruction = ("F", 11)
        location = (170, 38)
        waypoint = (4, -10)

        new_location, new_waypoint = get_location_and_waypoint(
            instruction, location, waypoint
        )
        self.assertEqual((214, -72), new_location)
        self.assertEqual((4, -10), new_waypoint)

    def test_process_instructions_with_new_instructions(self):
        instructions = parse_input(test_input)
        result = process_instructions_with_new_instructions(instructions)
        self.assertEqual((214, -72), result)
