from unittest import TestCase

from advent_2020.day_2.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        expected = [
            (1, 3, "a", "abcde"),
            (1, 3, "b", "cdefg"),
            (2, 9, "c", "ccccccccc"),
        ]
        self.assertEqual(expected, result)

    def test_count_validation_eg_one(self):
        result = validate_using_count_method(1, 3, "a", "abcde")
        self.assertEqual(True, result)

    def test_count_validation_eg_two(self):
        result = validate_using_count_method(1, 3, "b", "cdefg")
        self.assertEqual(False, result)

    def test_count_validation_eg_three(self):
        result = validate_using_count_method(2, 9, "c", "ccccccccc")
        self.assertEqual(True, result)

    def test_can_count_validation_using_count_method(self):
        groups = parse_input(test_input)
        result = num_of_valid_passwords_using_count_method(groups)
        self.assertEqual(2, result)

    def test_xor_validation_eg_one(self):
        result = validate_using_xor_method(1, 3, "a", "abcde")
        self.assertEqual(True, result)

    def test_xor_validation_eg_two(self):
        result = validate_using_xor_method(1, 3, "b", "cdefg")
        self.assertEqual(False, result)

    def test_xor_validation_eg_three(self):
        result = validate_using_xor_method(2, 9, "c", "ccccccccc")
        self.assertEqual(False, result)

    def test_can_count_validation_using_xor_method(self):
        groups = parse_input(test_input)
        result = num_of_valid_passwords_using_xor_method(groups)
        self.assertEqual(1, result)
