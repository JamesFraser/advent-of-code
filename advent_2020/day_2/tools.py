import re
from typing import Callable, List, Tuple

Group = Tuple[int, int, str, str]


def parse_input(input_text: str) -> List[Group]:
    regex = "^(\d+)-(\d+)\s+(.+?):\s(.+?)$"

    input_groups = []
    for line in input_text.splitlines():
        m = re.search(regex, line.strip())
        input_groups.append((int(m.group(1)), int(m.group(2)), m.group(3), m.group(4)))
    return input_groups


def validate_using_count_method(
    min_limit: int, max_limit: int, character: str, input_string: str
) -> bool:
    count = input_string.count(character)
    return min_limit <= count <= max_limit


def num_of_valid_passwords_using_count_method(groups: List[Group]) -> int:
    return len([True for group in groups if validate_using_count_method(*group)])


def validate_using_xor_method(
    lower: int, upper: int, character: str, input_string: str
) -> bool:
    min_match = input_string[lower - 1] == character
    max_match = input_string[upper - 1] == character
    return min_match != max_match


def num_of_valid_passwords_using_xor_method(groups: List[Group]) -> int:
    return len([group for group in groups if validate_using_xor_method(*group)])


def num_of_valid_passwords_using_method(
    groups: List[Group], validate_method: Callable
) -> int:
    return len([group for group in groups if validate_method(*group)])


def handle_part_one(input_text: str) -> int:
    groups = parse_input(input_text)
    return num_of_valid_passwords_using_count_method(groups)


def handle_part_two(input_text: str) -> int:
    groups = parse_input(input_text)
    return num_of_valid_passwords_using_xor_method(groups)
