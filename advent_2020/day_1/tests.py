from unittest import TestCase

from advent_2020.day_1.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        self.assertEqual([1721, 979, 366, 299, 675, 1456], result)

    def test_can_find_known_pair(self):
        input_list = parse_input(test_input)
        result = find_group_sums_to(input_list, 2020, 2)
        self.assertEqual({299, 1721}, result)

    def test_can_get_correct_part_one(self):
        result = get_product_of_group_sums_to(test_input, 2020, 2)
        self.assertEqual(514579, result)

    def test_can_find_known_triplet(self):
        input_list = parse_input(test_input)
        result = find_group_sums_to(input_list, 2020, 3)
        self.assertEqual({366, 675, 979}, result)

    def test_can_get_correct_part_two(self):
        result = get_product_of_group_sums_to(test_input, 2020, 3)
        self.assertEqual(241861950, result)
