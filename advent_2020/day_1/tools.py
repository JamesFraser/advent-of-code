from functools import reduce
from itertools import permutations
from typing import List, Set


def parse_input(input_text: str) -> List[int]:
    return [int(m) for m in input_text.splitlines()]


def find_group_sums_to(input_list: List[int], target: int, set_size: int) -> Set[int]:
    all_groups = permutations(input_list, set_size)
    for group in all_groups:
        if sum(group) == target:
            return set(group)


def get_product_of_group_sums_to(input_text: str, target: int, set_size: int) -> int:
    input_list = parse_input(input_text)
    group = list(find_group_sums_to(input_list, target, set_size))
    return reduce((lambda x, y: x * y), group)


def handle_part_one(input_text: str) -> int:
    return get_product_of_group_sums_to(input_text, 2020, 2)


def handle_part_two(input_text: str) -> int:
    return get_product_of_group_sums_to(input_text, 2020, 3)
