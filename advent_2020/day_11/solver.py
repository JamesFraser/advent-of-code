from advent_2020.day_11.tools import *

if __name__ == "__main__":
    f = open("puzzle_input.txt", "r")
    puzzle_input = f.read()

    print("Part 1:")
    print(handle_part_one(puzzle_input))
    print("")
    print("Part 2:")
    print(handle_part_two(puzzle_input))
