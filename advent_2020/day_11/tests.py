from unittest import TestCase

from advent_2020.day_11.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input, 4, 1)
        self.assertEqual(2, len(result[0].adjacent))

    def test_run_until_stable(self):
        seats = parse_input(test_input, 4, 1)
        seats = run_until_stable(seats)
        result = len([seat for seat in seats if seat.occupied])
        self.assertEqual(37, result)

    def test_parse_input_part_two(self):
        result = parse_input(test_input, 5, None)
        self.assertEqual(3, len(result[0].adjacent))

    def test_run_until_stable_part_two(self):
        seats = parse_input(test_input, 5, None)
        seats = run_until_stable(seats)
        result = len([seat for seat in seats if seat.occupied])
        self.assertEqual(26, result)
