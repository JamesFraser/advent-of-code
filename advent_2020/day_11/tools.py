from typing import List


class Seat:
    def __init__(self, seating_tolerance: int = 4):
        self.occupied = False
        self.adjacent = []
        self.occupied_next_round = None
        self.seating_tolerance = seating_tolerance

    def determine_next_round(self):
        self.occupied_next_round = self.occupied

        occupied_adjacent_seats = len([seat for seat in self.adjacent if seat.occupied])

        if self.occupied:
            if occupied_adjacent_seats >= self.seating_tolerance:
                self.occupied_next_round = False
        else:
            if occupied_adjacent_seats == 0:
                self.occupied_next_round = True

    def next_round(self):
        self.occupied = self.occupied_next_round
        self.occupied_next_round = None


def parse_input(
    input_text: str, seating_tolerance: int, search_distance: int = None
) -> List[Seat]:
    rows = input_text.splitlines()
    height = len(rows)
    length = len(rows[0])

    seats = []

    seat_map = {}

    for y in range(height):
        for x in range(length):
            if rows[y][x] == ".":
                continue

            seat = Seat(seating_tolerance)

            if rows[y][x] == "#":
                seat.occupied = True

            seat_map[(x, y)] = seat
            seats.append(seat)

    for (x, y), seat in seat_map.items():
        for x_delta, y_delta in [
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ]:
            if not search_distance:
                search_distance = max([height, length])

            for r in range(1, search_distance + 1):
                search_x = x + (r * x_delta)
                search_y = y + (r * y_delta)

                if not 0 <= search_x < length:
                    break
                if not 0 <= search_y < height:
                    break

                if (search_x, search_y) in seat_map:
                    seat.adjacent.append(seat_map[(search_x, search_y)])
                    break

    return seats


def run_until_stable(seats: List[Seat]) -> List[Seat]:
    while True:
        states_before = [seat.occupied for seat in seats]

        for seat in seats:
            seat.determine_next_round()

        for seat in seats:
            seat.next_round()

        states_after = [seat.occupied for seat in seats]

        if states_before == states_after:
            return seats


def handle_part_one(input_text: str) -> int:
    seats = parse_input(input_text, seating_tolerance=4, search_distance=1)
    seats = run_until_stable(seats)
    return len([seat for seat in seats if seat.occupied])


def handle_part_two(input_text: str) -> int:
    seats = parse_input(input_text, seating_tolerance=5, search_distance=None)
    seats = run_until_stable(seats)
    return len([seat for seat in seats if seat.occupied])
