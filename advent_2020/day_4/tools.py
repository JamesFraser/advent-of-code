import re
from typing import Dict, List


def parse_input(input_text: str) -> List[Dict]:
    batches = []
    current_batch = {}
    for line in input_text.splitlines():
        if not line:
            batches.append(current_batch)
            current_batch = {}
        else:
            for chunk in line.split(" "):
                key, val = chunk.split(":")
                current_batch[key] = val
    batches.append(current_batch)
    return batches


def is_passport_valid(passport: Dict) -> bool:
    for field in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]:
        if field not in passport.keys():
            return False
    return True


def count_valid_passports(passports, strict=False):
    valid_count = 0
    for passport in passports:
        if strict:
            if is_passport_valid_strict(passport):
                valid_count += 1
        else:
            if is_passport_valid(passport):
                valid_count += 1
    return valid_count


def _strict_validate_byr(byr: str) -> bool:
    if len(byr) != 4:
        return False

    return 1920 <= int(byr) <= 2002


def _strict_validate_iyr(iyr: str) -> bool:
    if len(iyr) != 4:
        return False

    return 2010 <= int(iyr) <= 2020


def _strict_validate_eyr(eyr: str) -> bool:
    if len(eyr) != 4:
        return False

    return 2020 <= int(eyr) <= 2030


def _strict_validate_hgt(hgt: str) -> bool:
    unit = hgt[-2:]
    if unit not in ["cm", "in"]:
        return False

    try:
        height = int(hgt[:-2])
    except:
        return False

    if unit == "cm":
        return 150 <= height <= 193
    if unit == "in":
        return 59 <= height <= 76

    return False


def _strict_validate_hcl(hcl: str) -> bool:
    hex_regex = "^#([A-Fa-f0-9]{6})$"
    return bool(re.match(hex_regex, hcl))


def _strict_validate_ecl(ecl: str) -> bool:
    return ecl in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]


def _strict_validate_pid(pid: str) -> bool:
    pid_regex = "^([0-9]{9})$"
    return bool(re.match(pid_regex, pid))


def is_passport_valid_strict(passport: Dict) -> bool:
    field_validators = {
        "byr": _strict_validate_byr,
        "iyr": _strict_validate_iyr,
        "eyr": _strict_validate_eyr,
        "hgt": _strict_validate_hgt,
        "hcl": _strict_validate_hcl,
        "ecl": _strict_validate_ecl,
        "pid": _strict_validate_pid,
    }

    for field, validator in field_validators.items():
        if field not in passport.keys():
            return False
        if not validator(passport[field]):
            return False

    return True


def handle_part_one(input_text: str) -> int:
    passports = parse_input(input_text)
    return count_valid_passports(passports)


def handle_part_two(input_text: str) -> int:
    passports = parse_input(input_text)
    return count_valid_passports(passports, strict=True)
