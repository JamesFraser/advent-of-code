from unittest import TestCase

from advent_2020.day_4.tools import *
from advent_2020.day_4.tools import (
    _strict_validate_byr,
    _strict_validate_ecl,
    _strict_validate_hcl,
    _strict_validate_hgt,
    _strict_validate_pid,
)

f = open("test_input.txt", "r")
test_input = f.read()


f = open("test_input_all_valid.txt", "r")
valid_test_input = f.read()


f = open("test_input_all_invalid.txt", "r")
invalid_test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)

        self.assertEqual(4, len(result))
        expected_first = {
            "ecl": "gry",
            "pid": "860033327",
            "eyr": "2020",
            "hcl": "#fffffd",
            "byr": "1937",
            "iyr": "2017",
            "cid": "147",
            "hgt": "183cm",
        }

        self.assertEqual(expected_first, result[0])

    def test_passport_is_valid_eg_1(self):
        passport = {
            "ecl": "gry",
            "pid": "860033327",
            "eyr": "2020",
            "hcl": "#fffffd",
            "byr": "1937",
            "iyr": "2017",
            "cid": "147",
            "hgt": "183cm",
        }
        result = is_passport_valid(passport)
        self.assertEqual(True, result)

    def test_passport_is_valid_eg_2(self):
        passport = {
            "iyr": "2013",
            "ecl": "amb",
            "cid": "350",
            "eyr": "2023",
            "pid": "028048884",
            "hcl": "#cfa07d",
            "byr": "1929",
        }
        result = is_passport_valid(passport)
        self.assertEqual(False, result)

    def test_passport_is_valid_eg_3(self):
        passport = {
            "hcl": "#ae17e1",
            "iyr": "2013",
            "eyr": "2024",
            "ecl": "brn",
            "pid": "760753108",
            "byr": "1931",
            "hgt": "179cm",
        }
        result = is_passport_valid(passport)
        self.assertEqual(True, result)

    def test_passport_is_valid_eg_4(self):
        passport = {
            "hcl": "#cfa07d",
            "eyr": "2025",
            "pid": "166559648",
            "iyr": "2011",
            "ecl": "brn",
            "hgt": "59in",
        }
        result = is_passport_valid(passport)
        self.assertEqual(False, result)

    def test_count_valid_passports(self):
        passports = parse_input(test_input)
        result = count_valid_passports(passports)
        self.assertEqual(2, result)

    def test_strict_validate_byr_eg_1(self):
        result = _strict_validate_byr("2002")
        self.assertEqual(True, result)

    def test_strict_validate_byr_eg_2(self):
        result = _strict_validate_byr("2003")
        self.assertEqual(False, result)

    def test_strict_validate_hgt_eg_1(self):
        result = _strict_validate_hgt("60in")
        self.assertEqual(True, result)

    def test_strict_validate_hgt_eg_2(self):
        result = _strict_validate_hgt("190cm")
        self.assertEqual(True, result)

    def test_strict_validate_hgt_eg_3(self):
        result = _strict_validate_hgt("190in")
        self.assertEqual(False, result)

    def test_strict_validate_hgt_eg_4(self):
        result = _strict_validate_hgt("190")
        self.assertEqual(False, result)

    def test_strict_validate_hcl_eg_1(self):
        result = _strict_validate_hcl("#123abc")
        self.assertEqual(True, result)

    def test_strict_validate_hcl_eg_2(self):
        result = _strict_validate_hcl("#123abz")
        self.assertEqual(False, result)

    def test_strict_validate_hcl_eg_3(self):
        result = _strict_validate_hcl("123abc")
        self.assertEqual(False, result)

    def test_strict_validate_ecl_eg_1(self):
        result = _strict_validate_ecl("brn")
        self.assertEqual(True, result)

    def test_strict_validate_ecl_eg_2(self):
        result = _strict_validate_ecl("wat")
        self.assertEqual(False, result)

    def test_strict_validate_pid_eg_1(self):
        result = _strict_validate_pid("000000001")
        self.assertEqual(True, result)

    def test_strict_validate_pid_eg_2(self):
        result = _strict_validate_pid("0123456789")
        self.assertEqual(False, result)

    def test_valid_passports_strict(self):
        passports = parse_input(valid_test_input)
        for passport in passports:
            result = is_passport_valid_strict(passport)
            self.assertEqual(True, result)

    def test_invalid_passports_strict(self):
        passports = parse_input(invalid_test_input)
        for passport in passports:
            result = is_passport_valid_strict(passport)
            self.assertEqual(False, result)

    def test_count_valid_passports_strict(self):
        passports = parse_input(valid_test_input)
        result = count_valid_passports(passports, strict=True)
        self.assertEqual(4, result)

    def test_count_invalid_passports_strict(self):
        passports = parse_input(invalid_test_input)
        result = count_valid_passports(passports, strict=True)
        self.assertEqual(0, result)
