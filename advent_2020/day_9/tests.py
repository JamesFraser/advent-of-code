from unittest import TestCase

from advent_2020.day_9.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_check_preamble_can_sum_eg_1(self):
        result = check_preamble_can_sum([35, 20, 15, 25, 47], 40)
        self.assertEqual(True, result)

    def test_check_preamble_can_sum_eg_2(self):
        result = check_preamble_can_sum([95, 102, 117, 150, 182], 127)
        self.assertEqual(False, result)

    def test_generate_preamble_and_target(self):
        input_list = parse_input(test_input)
        generator = generate_preamble_and_target(input_list, 5)

        result = next(generator)
        expected = ([35, 20, 15, 25, 47], 40)
        self.assertEqual(expected, result)

        result = next(generator)
        expected = ([20, 15, 25, 47, 40], 62)
        self.assertEqual(expected, result)

    def test_find_first_invalid_target(self):
        input_list = parse_input(test_input)
        result = find_first_invalid_target(input_list, 5)
        self.assertEqual(127, result)

    def test_find_contiguous_summing_set(self):
        input_list = parse_input(test_input)
        result = find_contiguous_summing_set(input_list, 127)
        self.assertEqual([15, 25, 47, 40], result)
