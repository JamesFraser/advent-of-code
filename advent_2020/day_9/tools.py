from itertools import permutations
from typing import List, Tuple


def parse_input(input_text: str) -> List[int]:
    return [int(line.strip()) for line in input_text.splitlines()]


def check_preamble_can_sum(preamble: List[int], target: int):
    perms = permutations(preamble, 2)
    for perm in perms:
        if sum(perm) == target:
            return True
    return False


def generate_preamble_and_target(
    input_list: List[int], preamble_size: int
) -> Tuple[List[int], int]:
    num_of_preambles = (len(input_list) - preamble_size) - 1
    for i in range(num_of_preambles):
        yield input_list[i : i + preamble_size], input_list[i + preamble_size]


def find_first_invalid_target(input_list: List[int], preamble_size: int) -> int:
    generator = generate_preamble_and_target(input_list, preamble_size)

    while generator:
        preamble, target = next(generator)
        if not check_preamble_can_sum(preamble, target):
            return target


def find_contiguous_summing_set(input_list: List[int], target: int) -> List[int]:
    for i, start in enumerate(input_list):
        sum_list = [start]
        for j in range(i + 1, len(input_list)):
            if sum(sum_list) == target:
                return sum_list
            if sum(sum_list) > target:
                break

            sum_list.append(input_list[j])


def handle_part_one(input_text: str) -> int:
    input_list = parse_input(input_text)
    return find_first_invalid_target(input_list, 25)


def handle_part_two(input_text: str) -> int:
    input_list = parse_input(input_text)
    sum_list = find_contiguous_summing_set(input_list, 258585477)
    return min(sum_list) + max(sum_list)
