from unittest import TestCase

from advent_2020.day_3.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_count_trees_along_slope_eg_1(self):
        result = count_trees_along_slope(test_input, (3, 1))
        self.assertEqual(7, result)

    def test_count_trees_along_slope_eg_2(self):
        result = count_trees_along_slope(test_input, (1, 1))
        self.assertEqual(2, result)

    def test_count_trees_along_slope_eg_3(self):
        result = count_trees_along_slope(test_input, (5, 1))
        self.assertEqual(3, result)

    def test_count_trees_along_slope_eg_4(self):
        result = count_trees_along_slope(test_input, (7, 1))
        self.assertEqual(4, result)

    def test_count_trees_along_slope_eg_5(self):
        result = count_trees_along_slope(test_input, (1, 2))
        self.assertEqual(2, result)

    def test_count_trees_along_slopes(self):
        routes = [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2),
        ]
        result = count_trees_along_slopes(test_input, routes)
        expected = [2, 7, 3, 4, 2]
        self.assertEqual(expected, result)

    def test_multiply_counted_trees(self):
        routes = [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2),
        ]
        result = multiply_trees_counted_along_slopes(test_input, routes)
        self.assertEqual(336, result)
