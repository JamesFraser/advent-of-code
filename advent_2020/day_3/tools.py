from functools import reduce
from typing import List, Tuple

Slope = Tuple[int, int]


def parse_input(input_text: str) -> List[str]:
    return [line.strip() for line in input_text.splitlines()]


def count_trees_along_slope(input_text: str, slope: Slope) -> int:
    rows = parse_input(input_text)
    x_delta, y_delta = slope

    x = 0
    y = 0

    count = 0
    while y < len(rows):
        if rows[y][x] == "#":
            count += 1
        x += x_delta
        x = x % (len(rows[0]))
        y += y_delta

    return count


def count_trees_along_slopes(input_text: str, slopes: List[Slope]) -> List[int]:
    return [count_trees_along_slope(input_text, slope) for slope in slopes]


def multiply_trees_counted_along_slopes(input_text: str, slopes: List[Slope]) -> int:
    trees_counted = count_trees_along_slopes(input_text, slopes)
    return reduce((lambda x, y: x * y), trees_counted)


def handle_part_one(input_text: str) -> int:
    return count_trees_along_slope(input_text, (3, 1))


def handle_part_two(input_text: str) -> int:
    slopes = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ]
    return multiply_trees_counted_along_slopes(input_text, slopes)
