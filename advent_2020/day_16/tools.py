from typing import Dict, List, Set, Tuple

Ticket = List[int]
Fields = Dict[str, Set[int]]


def parse_input(input_text: str) -> Tuple[Fields, Ticket, List[Ticket]]:
    fields = {}

    reading_fields = True
    reading_your_ticket = False
    reading_nearby_tickets = False

    your_ticket = None
    nearby_tickets = []

    for line in input_text.splitlines():
        if reading_fields:
            if line:
                field, values = line.split(": ")
                ranges = values.split(" or ")

                accepted_values = set()
                for r in ranges:
                    lower = int(r.split("-")[0])
                    upper = int(r.split("-")[1])
                    accepted_values.update(list(range(lower, upper + 1)))
                    fields[field] = accepted_values
            else:
                reading_fields = False
        else:
            if reading_your_ticket:
                your_ticket = [int(num) for num in line.split(",")]
                reading_your_ticket = False
            if reading_nearby_tickets:
                nearby_tickets.append([int(num) for num in line.split(",")])

            if line == "your ticket:":
                reading_your_ticket = True
            if line == "nearby tickets:":
                reading_nearby_tickets = True

    return fields, your_ticket, nearby_tickets


def get_invalid_values_for_ticket(ticket: Ticket, fields: Fields) -> List[int]:
    invalid_values = []
    for value in ticket:
        if not any(value in sublist for sublist in fields.values()):
            invalid_values.append(value)
    return invalid_values


def get_invalid_values_for_tickets(tickets: List[Ticket], fields: Fields) -> List[int]:
    invalid_values = []
    for ticket in tickets:
        invalid_values.extend(get_invalid_values_for_ticket(ticket, fields))
    return invalid_values


def purge_invalid_tickets(tickets: List[Ticket], fields: Fields) -> List[Ticket]:
    return [
        ticket
        for ticket in tickets
        if not get_invalid_values_for_ticket(ticket, fields)
    ]


def get_field_values_from_tickets(tickets: List[Ticket]) -> List[Set[int]]:
    return [set(zipped) for zipped in zip(*tickets)]


def determine_fields(tickets: List[Ticket], fields: Fields) -> List[str]:
    field_values = get_field_values_from_tickets(tickets)

    unmapped_fields = list(fields.keys())
    mapped_fields = [None for _ in range(len(fields))]

    while unmapped_fields:
        for i, column in enumerate(field_values):
            if mapped_fields[i]:
                continue
            possible_fields = []

            for key, value in fields.items():
                if key in mapped_fields:
                    continue
                if column.issubset(set(value)):
                    possible_fields.append(key)

            if len(possible_fields) == 1:
                mapped_fields[i] = possible_fields[0]
                unmapped_fields.remove(possible_fields[0])

    return mapped_fields


def map_fields_to_ticket(mappings: List[str], ticket: Ticket) -> Dict[str, int]:
    return {field: value for field, value in zip(mappings, ticket)}


def handle_part_one(input_text: str) -> int:
    fields, _, nearby_tickets = parse_input(input_text)

    invalid_values = get_invalid_values_for_tickets(nearby_tickets, fields)
    return sum(invalid_values)


def handle_part_two(input_text: str) -> int:
    fields, your_ticket, nearby_tickets = parse_input(input_text)
    nearby_tickets = purge_invalid_tickets(nearby_tickets, fields)

    mappings = determine_fields(nearby_tickets, fields)
    mapped_ticket = map_fields_to_ticket(mappings, your_ticket)

    product = 1
    for field in [
        "departure location",
        "departure station",
        "departure platform",
        "departure track",
        "departure date",
        "departure time",
    ]:
        product *= mapped_ticket[field]
    return product
