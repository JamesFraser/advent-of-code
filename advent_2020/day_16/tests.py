from unittest import TestCase

from advent_2020.day_16.tools import *

f = open("test_input.txt", "r")
test_input = f.read()

f = open("test_input_2.txt", "r")
test_input_2 = f.read()


class TestCases(TestCase):
    def test_parse_input_for_fields(self):
        fields, _, _ = parse_input(test_input)

        self.assertEqual(3, len(fields))
        self.assertEqual({1, 2, 3, 5, 6, 7}, fields["class"])
        self.assertEqual(
            {6, 7, 8, 9, 10, 11, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44},
            fields["row"],
        )

    def test_parse_input_for_your_ticket(self):
        _, your_ticket, _ = parse_input(test_input)

        self.assertEqual([7, 1, 14], your_ticket)

    def test_parse_input_for_nearby_tickets(self):
        _, _, nearby_tickets = parse_input(test_input)

        expected = [
            [7, 3, 47],
            [40, 4, 50],
            [55, 2, 20],
            [38, 6, 12],
        ]
        self.assertEqual(expected, nearby_tickets)

    def test_get_invalid_values_for_tickets(self):
        fields, _, nearby_tickets = parse_input(test_input)

        result = get_invalid_values_for_tickets(nearby_tickets, fields)
        self.assertEqual([4, 55, 12], result)

    def test_purge_invalid_tickets(self):
        fields, _, nearby_tickets = parse_input(test_input)
        result = purge_invalid_tickets(nearby_tickets, fields)
        self.assertEqual([[7, 3, 47]], result)

    def test_determine_fields(self):
        fields, _, nearby_tickets = parse_input(test_input_2)
        result = determine_fields(nearby_tickets, fields)
        self.assertEqual(["row", "class", "seat"], result)

    def test_map_fields_to_ticket(self):
        _, your_ticket, _ = parse_input(test_input_2)
        mappings = ["row", "class", "seat"]
        result = map_fields_to_ticket(mappings, your_ticket)
        self.assertEqual({"row": 11, "class": 12, "seat": 13}, result)

    def test_get_field_values_from_tickets(self):
        _, _, nearby_tickets = parse_input(test_input_2)
        result = get_field_values_from_tickets(nearby_tickets)
        self.assertEqual([{3, 15, 5}, {9, 1, 14}, {18, 5, 9}], result)
