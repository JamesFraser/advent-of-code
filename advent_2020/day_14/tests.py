from unittest import TestCase

from advent_2020.day_14.tools import *

f = open("test_input.txt", "r")
test_input = f.read()

f = open("test_input_2.txt", "r")
test_input_2 = f.read()


class TestCases(TestCase):
    def test_to_binary(self):
        result = to_binary(11)
        expected = list("000000000000000000000000000000001011")
        self.assertEqual(expected, result)

    def test_to_int(self):
        result = to_int(list("000000000000000000000000000000001011"))
        self.assertEqual(11, result)

    def test_run_program(self):
        program = parse_input(test_input)
        result = run_program(program)
        self.assertEqual(101, result[7])
        self.assertEqual(64, result[8])

    def test_parse_memory(self):
        program = parse_input(test_input)
        memory = run_program(program)
        result = parse_memory(memory)
        self.assertEqual(165, result)

    def test_run_program_v2(self):
        program = parse_input(test_input_2)
        result = run_program(program, use_memory_address_decoder=True)
        self.assertEqual(100, result[58])
        self.assertEqual(100, result[59])
        self.assertEqual(1, result[16])
        self.assertEqual(1, result[17])
        self.assertEqual(1, result[18])
        self.assertEqual(1, result[19])
        self.assertEqual(1, result[24])
        self.assertEqual(1, result[25])
        self.assertEqual(1, result[26])
        self.assertEqual(1, result[27])

    def test_parse_memory_v2(self):
        program = parse_input(test_input_2)
        memory = run_program(program, use_memory_address_decoder=True)
        result = parse_memory(memory)
        self.assertEqual(208, result)
