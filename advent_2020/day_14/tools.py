from itertools import product
from typing import Dict, List, Tuple, Union

Mask = List[str]
MemoryWrite = Tuple[int, int]
Program = List[Union[Mask, MemoryWrite]]


def to_binary(number: int) -> List[str]:
    s = bin(number)
    s = s.replace("0b", "")
    while len(s) < 36:
        s = "0" + s
    return list(s)


def to_int(binary: List[str]) -> int:
    bin_str = "".join(str(b) for b in binary)
    return int(bin_str, 2)


def parse_input(input_text: str) -> Program:
    program = []

    for line in input_text.splitlines():
        left, right = line.split(" = ")
        if left == "mask":
            program.append(right)
        else:
            mem_index = int(left.replace("mem[", "").replace("]", ""))
            value = int(right)
            program.append((mem_index, value))

    return program


def run_program(program: Program, use_memory_address_decoder=False) -> Dict[int, int]:
    mask = None
    memory = {}

    for line in program:
        if isinstance(line, str):
            mask = line
        elif isinstance(line, Tuple):
            memory_index, value = line

            if use_memory_address_decoder:
                binary = to_binary(memory_index)

                x_indexes = []
                for i, bit in enumerate(mask):
                    if bit == "1":
                        binary[i] = bit
                    if bit == "X":
                        x_indexes.append(i)

                options = product(range(2), repeat=len(x_indexes))
                for option in options:
                    b = binary.copy()
                    for option_idx, binary_idx in enumerate(x_indexes):
                        b[binary_idx] = option[option_idx]
                    memory_index = to_int(b)
                    memory[memory_index] = value
            else:
                binary = to_binary(value)

                for i, bit in enumerate(mask):
                    if bit != "X":
                        binary[i] = bit

                value = to_int(binary)
                memory[memory_index] = value

    return memory


def parse_memory(memory: Dict[int, int]) -> int:
    return sum(memory.values())


def handle_part_one(input_text: str) -> int:
    program = parse_input(input_text)
    memory = run_program(program)
    return parse_memory(memory)


def handle_part_two(input_text: str) -> int:
    program = parse_input(input_text)
    memory = run_program(program, use_memory_address_decoder=True)
    return parse_memory(memory)
