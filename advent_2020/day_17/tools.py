from itertools import product
from typing import Dict, List, Tuple


class ThreeDimensionalPocketDimension:
    def __init__(self, width, height, depth):
        self.width = width
        self.height = height
        self.depth = depth
        self.cubes = {
            (x, y, z): Cube()
            for x in range(width)
            for y in range(height)
            for z in range(depth)
        }

    def get(self, x, y, z):
        return self.cubes[(x, y, z)]

    @property
    def all_cubes(self):
        return self.cubes.values()

    def __repr__(self):
        s = "-" * 20
        s += "\n"
        for z in range(self.depth):
            for y in range(self.height):
                row = [self.get(x, y, z) for x in range(self.width)]

                s += "".join(["#" if cube.active else "." for cube in row])
                s += "\n"
            s += "\n"
        s += "-" * 20
        s += "\n"
        return s


class FourDimensionalPocketDimension:
    def __init__(self, w_size, x_size, y_size, z_size):
        self.w_size = w_size
        self.x_size = x_size
        self.y_size = y_size
        self.z_size = z_size

        self.cubes = {
            (w, x, y, z): Cube()
            for w in range(w_size)
            for x in range(x_size)
            for y in range(y_size)
            for z in range(z_size)
        }

    def get(self, w, x, y, z):
        return self.cubes[(w, x, y, z)]

    @property
    def all_cubes(self):
        return self.cubes.values()

    # def __repr__(self):
    #     s = '-' * 20
    #     s += '\n'
    #     for z in range(self.depth):
    #         for y in range(self.height):
    #             row = [self.get(x,y,z) for x in range(self.width)]
    #
    #             s += ''.join(['#' if cube.active else '.' for cube in row])
    #             s += '\n'
    #         s += '\n'
    #     s += '-' * 20
    #     s += '\n'
    #     return s


class Cube:
    def __init__(self):
        self.active = False
        self.adjacent = []
        self.active_next_round = None

    def determine_next_round(self):
        self.active_next_round = self.active

        active_adjacent_cubes = len([cube for cube in self.adjacent if cube.active])

        if self.active:
            if active_adjacent_cubes not in [2, 3]:
                self.active_next_round = False
        else:
            if active_adjacent_cubes == 3:
                self.active_next_round = True

    def next_round(self):
        self.active = self.active_next_round
        self.active_next_round = None


Coordinates = Tuple[int, int, int]


def parse_input_3d(input_text: str, cycles: int) -> ThreeDimensionalPocketDimension:
    adjacent_deltas = list(product(range(-1, 2), repeat=3))
    adjacent_deltas.remove((0, 0, 0))

    lines = input_text.splitlines()

    width = len(lines[0]) + (2 * cycles)
    height = len(lines) + (2 * cycles)
    depth = 1 + (2 * cycles)

    pocket_dimension = ThreeDimensionalPocketDimension(width, height, depth)

    # Initialise
    for y, line in enumerate(lines):
        for x, cell in enumerate(line):
            if cell == "#":
                pocket_dimension.get(cycles + x, cycles + y, cycles).active = True

    # Set adjacent
    for x in range(width):
        for y in range(height):
            for z in range(depth):
                cube = pocket_dimension.get(x, y, z)

                for x_delta, y_delta, z_delta in adjacent_deltas:
                    adj_x = x + x_delta
                    adj_y = y + y_delta
                    adj_z = z + z_delta

                    if not 0 <= adj_x < width:
                        continue
                    if not 0 <= adj_y < height:
                        continue
                    if not 0 <= adj_z < depth:
                        continue

                    cube.adjacent.append(pocket_dimension.get(adj_x, adj_y, adj_z))

                # print(f"{x}, {y}, {z}: {len(pocket_dimension.get(x,y,z).adjacent)}")
    return pocket_dimension


def parse_input_4d(input_text: str, cycles: int) -> FourDimensionalPocketDimension:
    adjacent_deltas = list(product(range(-1, 2), repeat=4))
    adjacent_deltas.remove((0, 0, 0, 0))

    lines = input_text.splitlines()

    x_size = len(lines[0]) + (2 * cycles)
    y_size = len(lines) + (2 * cycles)
    w_z_size = 1 + (2 * cycles)

    pocket_dimension = FourDimensionalPocketDimension(
        w_z_size, x_size, y_size, w_z_size
    )

    # Initialise
    for y, line in enumerate(lines):
        for x, cell in enumerate(line):
            if cell == "#":
                pocket_dimension.get(
                    cycles, cycles + x, cycles + y, cycles
                ).active = True

    # Set adjacent
    for w in range(w_z_size):
        for x in range(x_size):
            for y in range(y_size):
                for z in range(w_z_size):
                    cube = pocket_dimension.get(w, x, y, z)

                    for w_delta, x_delta, y_delta, z_delta in adjacent_deltas:
                        adj_w = w + w_delta
                        adj_x = x + x_delta
                        adj_y = y + y_delta
                        adj_z = z + z_delta

                        if not 0 <= adj_w < w_z_size:
                            continue
                        if not 0 <= adj_x < x_size:
                            continue
                        if not 0 <= adj_y < y_size:
                            continue
                        if not 0 <= adj_z < w_z_size:
                            continue

                        cube.adjacent.append(
                            pocket_dimension.get(adj_w, adj_x, adj_y, adj_z)
                        )

                # print(f"{x}, {y}, {z}: {len(pocket_dimension.get(x,y,z).adjacent)}")
    return pocket_dimension


def active_after_cycles(pocket_dimension, cycles: int) -> int:
    for _ in range(cycles):
        for cube in pocket_dimension.all_cubes:
            cube.determine_next_round()

        for cube in pocket_dimension.all_cubes:
            cube.next_round()

    active = 0
    for cube in pocket_dimension.all_cubes:
        if cube.active:
            active += 1
    return active


def handle_part_one(input_text: str) -> int:
    pocket_dimension = parse_input_3d(input_text, 6)
    return active_after_cycles(pocket_dimension, 6)


def handle_part_two(input_text: str) -> int:
    pocket_dimension = parse_input_4d(input_text, 6)
    return active_after_cycles(pocket_dimension, 6)
