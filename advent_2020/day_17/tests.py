from unittest import TestCase

from advent_2020.day_17.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_active_after_0_cycles(self):
        pocket_dimension = parse_input_3d(test_input, 0)
        result = active_after_cycles(pocket_dimension, 0)
        self.assertEqual(5, result)

    def test_active_after_1_cycle(self):
        pocket_dimension = parse_input_3d(test_input, 1)
        result = active_after_cycles(pocket_dimension, 1)
        self.assertEqual(11, result)

    def test_active_after_2_cycles(self):
        pocket_dimension = parse_input_3d(test_input, 2)
        result = active_after_cycles(pocket_dimension, 2)
        self.assertEqual(21, result)

    def test_active_after_3_cycles(self):
        pocket_dimension = parse_input_3d(test_input, 3)
        result = active_after_cycles(pocket_dimension, 3)
        self.assertEqual(38, result)

    def test_active_after_cycles(self):
        pocket_dimension = parse_input_3d(test_input, 6)
        result = active_after_cycles(pocket_dimension, 6)
        self.assertEqual(112, result)

    def test_active_after_cycles_4d(self):
        pocket_dimension = parse_input_4d(test_input, 6)
        result = active_after_cycles(pocket_dimension, 6)
        self.assertEqual(848, result)
