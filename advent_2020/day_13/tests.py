from unittest import TestCase

from advent_2020.day_13.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        earliest_departure, bus_ids = parse_input(test_input)
        self.assertEqual(939, earliest_departure)
        self.assertEqual([7, 13, 59, 31, 19], bus_ids)

    def test_get_timestamp_for_bus_id_after_timestamp_eg_1(self):
        result = get_timestamp_for_bus_id_after_timestamp(7, 939)
        self.assertEqual(945, result)

    def test_get_timestamp_for_bus_id_after_timestamp_eg_2(self):
        result = get_timestamp_for_bus_id_after_timestamp(59, 939)
        self.assertEqual(944, result)

    def test_get_earliest_bus_departure_after_timestamp(self):
        earliest_departure, bus_ids = parse_input(test_input)
        bus_id, departure = get_earliest_bus_departure_after_timestamp(
            bus_ids, earliest_departure
        )
        self.assertEqual(59, bus_id)
        self.assertEqual(944, departure)

    def test_parse_input_for_contest(self):
        result = parse_input_for_contest(test_input)
        expected = {7: 0, 13: -1, 59: -4, 31: -6, 19: -7}
        self.assertEqual(expected, result)

    def test_win_contest_eg_1(self):
        contest_data = parse_input_for_contest(test_input)
        result = win_contest(contest_data)
        self.assertEqual(1068781, result)

    def test_win_contest_eg_2(self):
        contest_data = {17: 0, 13: -2, 19: -3}
        result = win_contest(contest_data)
        self.assertEqual(3417, result)

    def test_win_contest_eg_3(self):
        contest_data = {67: 0, 7: -1, 59: -2, 61: -3}
        result = win_contest(contest_data)
        self.assertEqual(754018, result)

    def test_win_contest_eg_4(self):
        contest_data = {67: 0, 7: -2, 59: -3, 61: -4}
        result = win_contest(contest_data)
        self.assertEqual(779210, result)

    def test_win_contest_eg_5(self):
        contest_data = {67: 0, 7: -1, 59: -3, 61: -4}
        result = win_contest(contest_data)
        self.assertEqual(1261476, result)

    def test_win_contest_eg_6(self):
        contest_data = {1789: 0, 37: -1, 47: -2, 1889: -3}
        result = win_contest(contest_data)
        self.assertEqual(1202161486, result)

    def test_find_inverse_modulo(self):
        result = find_inverse_modulo(15, 26)
        self.assertEqual(7, result)

    def test_find_inverse_modulo_eg_2(self):
        result = find_inverse_modulo(28, 3)
        self.assertEqual(1, result)

    def test_win_contest_crt(self):
        contest_data = {3: 2, 4: 5, 7: -3}
        result = win_contest(contest_data)
        self.assertEqual(53, result)

    def test_win_contest_crt_eg_2(self):
        contest_data = {3: 2, 7: 3, 2: 0, 5: 0}
        result = win_contest(contest_data)
        self.assertEqual(80, result)

    def test_win_contest_crt_eg_3(self):
        contest_data = {2: 1, 3: 1, 4: 1, 5: 1, 7: 0}
        result = win_contest(contest_data)
        self.assertEqual(301, result)
