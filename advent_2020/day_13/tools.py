import math
from math import ceil
from typing import Dict, List, Tuple

import numpy as np


def parse_input(input_text: str) -> Tuple[int, List[int]]:
    earliest_departure = int(input_text.splitlines()[0])
    bus_ids = [int(x) for x in input_text.splitlines()[1].split(",") if x != "x"]
    return earliest_departure, bus_ids


def get_timestamp_for_bus_id_after_timestamp(bus_id: int, timestamp: int) -> int:
    x = ceil(timestamp / bus_id)
    return x * bus_id


def get_earliest_bus_departure_after_timestamp(
    bus_ids: List[int], timestamp: int
) -> Tuple[int, int]:
    bus = None
    earliest = None
    for bus_id in bus_ids:
        bus_departure = get_timestamp_for_bus_id_after_timestamp(bus_id, timestamp)
        if not earliest or bus_departure < earliest:
            earliest = bus_departure
            bus = bus_id

    return bus, earliest


def parse_input_for_contest(input_text: str) -> Dict[int, int]:
    bus_ids = {}
    for i, bus_id in enumerate(input_text.splitlines()[1].split(",")):
        if bus_id == "x":
            continue
        bus_ids[int(bus_id)] = -i

    return bus_ids


def find_inverse_modulo(i, mod):
    initial_mod = mod

    ps = []
    qs = []
    idx = 0
    while i != 0:
        q = math.floor(mod / i)
        qs.append(q)

        r = mod - (q * i)
        mod = i
        i = r

        if idx in [0, 1]:
            ps.append(idx)
        else:
            p = (ps[idx - 2] - (ps[idx - 1] * qs[idx - 2])) % initial_mod
            ps.append(p)

        idx += 1
    p = (ps[idx - 2] - (ps[idx - 1] * qs[idx - 2])) % initial_mod
    return p


def win_contest(contest_data: Dict[int, int]) -> int:
    lcm = np.lcm.reduce(list(contest_data.keys()))
    total = 0
    for modulo, remaining in contest_data.items():
        n = int(lcm / modulo)
        inverse_m = find_inverse_modulo(n, modulo)
        total += remaining * n * inverse_m
    return total % lcm


def handle_part_one(input_text: str) -> int:
    earliest_departure, bus_ids = parse_input(input_text)
    bus_id, departure = get_earliest_bus_departure_after_timestamp(
        bus_ids, earliest_departure
    )

    return bus_id * (departure - earliest_departure)


def handle_part_two(input_text: str) -> int:
    contest_data = parse_input_for_contest(input_text)
    return win_contest(contest_data)
