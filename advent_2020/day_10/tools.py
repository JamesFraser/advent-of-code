import operator
from itertools import groupby
from typing import Dict, List


def parse_input(input_text: str) -> List[int]:
    joltages = [int(line.strip()) for line in input_text.splitlines()]
    joltages.append(0)
    joltages = sorted(joltages)
    joltages.append(joltages[-1] + 3)
    return joltages


def get_jolt_differences(joltages: List[int]) -> Dict[int, int]:
    differences = list(map(operator.sub, joltages[1:], joltages[:-1]))

    count = {1: 0, 2: 0, 3: 0}
    for diff in differences:
        count[diff] += 1
    return count


def get_neighbour_pairs(joltage: int):
    return {
        (joltage - 2, joltage + 1),
        (joltage - 1, joltage + 1),
        (joltage - 1, joltage + 2),
    }


def get_skippable_blocks(joltages: List[int]) -> List[int]:
    skippables = []
    for rating in joltages[1:]:
        skippable = any(
            all(n in joltages for n in neighbour_pair)
            for neighbour_pair in get_neighbour_pairs(rating)
        )
        skippables.append(skippable)

    return [(sum(1 for _ in group)) for key, group in groupby(skippables) if key]


def determine_number_of_arrangements(joltages: List[int]) -> int:
    skippable_blocks = get_skippable_blocks(joltages)
    if max(skippable_blocks) > 3:
        raise NotImplementedError("This can only handle certain cases")

    num_arrangements = 1
    maths_map = {1: 2, 2: 4, 3: 7}

    for block in skippable_blocks:
        num_arrangements *= maths_map[block]
    return num_arrangements


def handle_part_one(input_text: str) -> int:
    joltages = parse_input(input_text)
    result = get_jolt_differences(joltages)
    return result[1] * result[3]


def handle_part_two(input_text: str) -> int:
    joltages = parse_input(input_text)
    return determine_number_of_arrangements(joltages)
