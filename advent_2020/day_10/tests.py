from unittest import TestCase

from advent_2020.day_10.tools import *

f = open("test_input.txt", "r")
test_input = f.read()

f = open("test_input_2.txt", "r")
test_input_2 = f.read()


class TestCases(TestCase):
    def test_get_jolt_differences_eg_1(self):
        joltages = parse_input(test_input)
        result = get_jolt_differences(joltages)
        self.assertEqual({1: 7, 2: 0, 3: 5}, result)

    def test_get_jolt_differences_eg_2(self):
        joltages = parse_input(test_input_2)
        result = get_jolt_differences(joltages)
        self.assertEqual({1: 22, 2: 0, 3: 10}, result)

    def test_get_neighbour_pairs(self):
        result = get_neighbour_pairs(5)
        expected = {
            (3, 6),
            (4, 6),
            (4, 7),
        }
        self.assertEqual(expected, result)

    def test_get_skippable_blocks_eg_1(self):
        joltages = parse_input(test_input)
        result = get_skippable_blocks(joltages)
        self.assertEqual([2, 1], result)

    def test_get_skippable_blocks_eg_2(self):
        joltages = parse_input(test_input_2)
        result = get_skippable_blocks(joltages)
        self.assertEqual([3, 3, 2, 1, 3, 3], result)

    def test_determine_number_of_arrangements_eg_1(self):
        joltages = parse_input(test_input)
        result = determine_number_of_arrangements(joltages)
        self.assertEqual(8, result)

    def test_determine_number_of_arrangements_eg_2(self):
        joltages = parse_input(test_input_2)
        result = determine_number_of_arrangements(joltages)
        self.assertEqual(19208, result)
