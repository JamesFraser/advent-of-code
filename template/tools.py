def parse_input(input_text: str):
    return [line.strip() for line in input_text.splitlines()]


def handle_part_one(input_text: str) -> int:
    pass


def handle_part_two(input_text: str) -> int:
    pass
