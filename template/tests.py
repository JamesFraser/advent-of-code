from unittest import TestCase

from advent_2020.day_x.tools import *

f = open("test_input.txt", "r")
test_input = f.read()


class TestCases(TestCase):
    def test_parse_input(self):
        result = parse_input(test_input)
        self.assertEqual(True, result)
